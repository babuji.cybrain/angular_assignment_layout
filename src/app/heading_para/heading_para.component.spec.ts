/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Heading_paraComponent } from './heading_para.component';

describe('Heading_paraComponent', () => {
  let component: Heading_paraComponent;
  let fixture: ComponentFixture<Heading_paraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Heading_paraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Heading_paraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
